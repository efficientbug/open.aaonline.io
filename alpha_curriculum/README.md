# Full Stack Online - Alpha Curriculum

## Notes

There isn't much to say about this course in particular. It served as a reminder
on Ruby, some concepts were kind of new to me but still easy to digest, such as
procs.

There isn't much code that can't be seem on this course's solution files and
walkthroughs -- which I ended up mostly skimming due to already knowing most of
what's covered.

## TODO

* [ ] Fix linting on these files
* [ ] Refactor some redundant code written throughout the course
* [ ] Add a table of contents to this file with the correct order & links.
