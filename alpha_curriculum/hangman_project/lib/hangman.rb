class Hangman
  DICTIONARY = ["cat", "dog", "bootcamp", "pizza"]

  def self.random_word
    DICTIONARY.sample
  end

  def initialize
    @secret_word = Hangman.random_word
    @guess_word = ["_"] * @secret_word.length
    @attempted_chars = []
    @remaining_incorrect_guesses = 5
  end

  def guess_word
    @guess_word
  end

  def attempted_chars
    @attempted_chars
  end

  def remaining_incorrect_guesses
    @remaining_incorrect_guesses
  end

  def already_attempted?(c)
    @attempted_chars.include?(c)
  end

  def get_matching_indices(c)
    @secret_word.chars.map.with_index { |ch, i| i if ch == c }.compact
  end

  def fill_indices(c, is)
    is.each { |i| @guess_word[i] = c }
  end

  def try_guess(c)
    if already_attempted?(c)
      puts "that has already been attempted"
      false
    else
      is = get_matching_indices(c)
      @attempted_chars << c
      if is.empty?
        @remaining_incorrect_guesses -= 1
      else
        fill_indices(c, is)
      end
      true
    end
  end

  def ask_user_for_guess
    print "Enter a char: "
    try_guess(gets.chomp)
  end

  def win?
    puts "WIN" if @guess_word.join == @secret_word
    @guess_word.join == @secret_word
  end

  def lose?
    puts "LOSE" if @remaining_incorrect_guesses == 0
    @remaining_incorrect_guesses == 0
  end

  def game_over?
    puts @secret_word if win? || lose?
    win? || lose?
  end
end
