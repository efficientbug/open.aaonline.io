def echo(s)
  s
end

def shout(s)
  s.upcase
end

def repeat(s, n = 2)
  ([s] * n).join(" ")
end

def start_of_word(s, n)
  s[0...n]
end

def first_word(s)
  s.split.first
end

LITTLE_WORDS = ["and", "the", "in", "over"]
def titleize(s)
  words = s.split.map.with_index do |word, i|
    if LITTLE_WORDS.include?(word) && i != 0
      word.downcase
    else
      word.capitalize
    end
  end

  words.join(" ")
end
