def pig_latin(word)
  if "aeiou".include?(word[0])
    word + "ay"
  else
    pigword = word.gsub(/([^aeiou]?qu|[^aeiou]+)([aeiou]\w*)/, "\\2\\1ay")
    word == word.capitalize ? pigword.capitalize : pigword
  end
end

def translate(s)
  s.split.map { |w| pig_latin(w) }.join(" ")
end
