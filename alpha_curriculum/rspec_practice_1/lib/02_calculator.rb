def add(a, b)
  a + b
end

def subtract(a, b)
  a - b
end

def multiply(*ns)
  [*ns].reduce(:*)
end

def power(a, b)
  a ** b
end

def factorial(n)
  n == 0 ? 1 : n.downto(1).reduce(:*)
end

def sum(arr)
  arr.sum
end
