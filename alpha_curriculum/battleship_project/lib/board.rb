class Board
  attr_reader :size

  def initialize(n)
    @grid = []
    @size = n * n
    @n = n
    n.times { @grid << [:N] * n }
  end

  def [](coords)
    i, j = coords
    @grid[i][j]
  end

  def []=(coords, value)
    i, j = coords
    @grid[i][j] = value
  end

  def num_ships
    @grid.flatten.count(:S)
  end

  def attack(coords)
    if self[coords] == :S
      puts "you sunk my battleship!"
      self[coords] = :H
      true
    else
      self[coords] = :X
      false
    end
  end

  def place_random_ships
    amount = self.size * 1/4
    amount.times do
      put_ship = false
      while !put_ship
        i, j = rand(@n), rand(@n)
        if @grid[i][j] != :S
          @grid[i][j] = :S
          put_ship = true
        end
      end
    end
  end

  def hidden_ships_grid
    @grid.map do |row|
      row.map { |cell| cell == :S ? :N : cell }
    end
  end

  def self.print_grid(grid)
    grid.each { |row| puts row.join(" ") }
  end

  def cheat
    Board.print_grid(@grid)
  end

  def print
    Board.print_grid(hidden_ships_grid)
  end
end
