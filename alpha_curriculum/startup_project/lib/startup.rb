require "employee"

class Startup
  attr_reader :name, :funding, :salaries, :employees

  def initialize(name, funding, salaries)
    @name = name
    @funding = funding
    @salaries = salaries
    @employees = []
  end

  def valid_title?(title)
    @salaries.key?(title)
  end

  def >(other_startup)
    self.funding > other_startup.funding
  end

  def hire(name, title)
    if valid_title?(title)
      @employees << Employee.new(name, title)
    else
      raise "invalid title"
    end
  end

  def size
    @employees.count
  end

  def pay_employee(employee)
    salary = @salaries[employee.title]
    if @funding >= salary
      employee.pay(salary)
      @funding -= salary
    else
      raise "insufficient funding"
    end
  end

  def payday
    @employees.each { |employee| pay_employee(employee) }
  end

  def average_salary
    total = @employees.each.collect { |emp| @salaries[emp.title] }.sum
    total.to_f / size
  end

  def close
    @employees = []
    @funding = 0
  end

  def acquire(other_startup)
    @funding += other_startup.funding
    other_startup.salaries.each do |title, salary|
      @salaries[title] = salary if !valid_title?(title)
    end
    @employees.concat(other_startup.employees)
    other_startup.close
  end
end
