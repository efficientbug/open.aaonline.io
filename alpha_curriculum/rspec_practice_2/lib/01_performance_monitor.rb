def measure(n = 1, &prc)
  started = Time.now
  n.times { prc.call }
  (Time.now - started) / n
end
