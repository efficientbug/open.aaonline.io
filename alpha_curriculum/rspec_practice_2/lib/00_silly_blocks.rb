def reverser(&prc)
  s = proc.call
  s.split.map(&:reverse).join(" ")
end

def adder(n = 1, &prc)
  proc.call + n
end

def repeater(n = 1, &prc)
  n.times { prc.call }
end
