class Code
  POSSIBLE_PEGS = {
    "R" => :red,
    "G" => :green,
    "B" => :blue,
    "Y" => :yellow
  }

  attr_reader :pegs

  def self.valid_pegs?(arr)
    arr.all? { |peg| POSSIBLE_PEGS.include?(peg.upcase) }
  end

  def self.random(length)
    Code.new([POSSIBLE_PEGS.keys.sample] * length)
  end

  def self.from_string(s)
    Code.new(s.chars)
  end

  def initialize(pegs)
    if Code.valid_pegs?(pegs)
      @pegs = pegs.map(&:upcase)
    else
      raise "invalid pegs"
    end
  end

  def [](i)
    @pegs[i]
  end

  def length
    @pegs.count
  end

  def num_exact_matches(code)
    @pegs.select.with_index { |peg, i| peg == code[i] }.count
  end

  def num_near_matches(code)
    near = @pegs.select.with_index do |peg, i|
        @pegs.include?(code[i]) && code[i] != @pegs[i]
    end
    near.count
  end

  def ==(other_code)
    length == num_exact_matches(other_code) && length == other_code.length
  end
end
