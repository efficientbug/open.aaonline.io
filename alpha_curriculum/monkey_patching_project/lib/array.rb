# Monkey-Patch Ruby's existing Array class to add your own custom methods
class Array
  def span
    return nil if self.empty?

    self.max - self.min
  end

  def average
    return nil if self.empty?

    self.sum.to_f / self.count
  end

  def median
    return nil if self.empty?

    mid = self.count / 2
    if self.count.even?
      self.sort[mid-1, 2].sum / 2.0
    else
      self.sort[mid]
    end
  end

  def counts
    self.each_with_object(Hash.new(0)) { |el, h| h[el] += 1 }
  end

  def my_count(item)
    self.select { |el| el == item }.count
  end

  def my_index(item)
    each_index do |i|
      return i if self[i] == item
    end

    nil
  end

  def my_uniq
    self.map { |el| [el, nil] }.to_h.keys
  end

  def my_transpose
    arr = []

    self.each_with_index do |row, i|
      row.each_with_index do |n, j|
        arr[j] = [] if arr[j].nil?
        arr[j][i] = self[i][j]
      end
    end

    arr
  end
end
